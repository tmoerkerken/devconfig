" Indentation
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 | 
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set colorcolumn=120 |

au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2



" Swap files
set nobackup
set noswapfile


" Plugins

call plug#begin('~/.vim/plugged')

    "-------------------=== Code/Project navigation ===-------------
    Plug 'majutsushi/tagbar'                  " Class/module browser
    Plug 'scrooloose/nerdtree'                " Project and file navigation
    "-------------------=== Other ===-------------------------------
    Plug 'bling/vim-airline'                  " Lean & mean status/tabline for vim
    Plug 'vim-airline/vim-airline-themes'     " Themes for airline
    Plug 'fisadev/FixedTaskList.vim'          " Pending tasks list
    Plug 'tpope/vim-surround'                 " Parentheses, brackets, quotes, XML tags, and more
    Plug 'flazz/vim-colorschemes'             " Colorschemes
    Plug 'miyakogi/seiya.vim'                 " Background
    Plug 'terryma/vim-expand-region'

    "-------------------=== Snippets support ===--------------------
    Plug 'garbas/vim-snipmate'                " Snippets manager
    Plug 'MarcWeber/vim-addon-mw-utils'       " dependencies #1
    Plug 'tomtom/tlib_vim'                    " dependencies #2
    Plug 'honza/vim-snippets'                 " snippets repo

    "-------------------=== Languages support ===-------------------
    Plug 'tpope/vim-commentary'               " Comment stuff out
    Plug 'Rykka/riv.vim'                      " ReStructuredText plugin

    "-------------------=== Python  ===-----------------------------
    Plug 'nvie/vim-flake8'

call plug#end()

set hlsearch
hi Search cterm=NONE ctermfg=grey ctermbg=yellow

"=====================================================
"" General settings
"=====================================================
syntax enable                               " syntax highlight

set t_Co=256                                " set 256 colors
colorscheme wombat256mod                    " set color scheme
let g:seiya_auto_enable=1


set number                                  " show line numbers
set nowrap
set ruler
set ttyfast                                 " terminal acceleration

set tabstop=4                               " 4 whitespaces for tabs visual presentation
set shiftwidth=4                            " shift lines by 4 spaces
set smarttab                                " set tabs for a shifttabs logic
set expandtab                               " expand tabs into spaces
set autoindent                              " indent when moving to the next line while writing code

set cursorline                              " shows line under the cursor's line
set showmatch                               " shows matching part of bracket pairs (), [], {}

set enc=utf-8	                            " utf-8 by default

set nobackup 	                            " no backup files
set nowritebackup                           " only in case you don't want a backup file while editing
set noswapfile 	                            " no swap files

set backspace=indent,eol,start              " backspace removes all (indents, EOLs, start) What is start?

set scrolloff=10                            " let 10 lines before/after cursor during scroll

set clipboard=unnamed                       " use system clipboard



" Breakpoints
"func! s:SetBreakpointPython()
"    cal append('.', repeat(' ', strlen(matchstr(getline('.'), '^\s*'))) . 'import ipdb; ipdb.set_trace()')
"endf
"
"func! s:RemoveBreakpointPython()
"    exe 'silent! g/^\s*import\sipdb\;\?\n*\s*ipdb.set_trace()/d'
"endf
"
"func! s:ToggleBreakpointPython()
"    if getline('.')=~#'^\s*import\sipdb' | cal s:RemoveBreakpointPython() | el | cal s:SetBreakpointPython() | en
"endf
"nnoremap <F6> :call <SID>ToggleBreakpointPython()<CR>
"
highlight ColorColumn ctermbg=0 guibg=lightgrey


" Auto expanding
map K <Plug>(expand_region_expand)
map J <Plug>(expand_region_shrink)
" virtualenv

" Linting
let g:flake8_show_in_gutter=1
let g:flake8_show_in_file=1
